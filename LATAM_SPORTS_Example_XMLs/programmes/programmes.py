# A script of migrating data from firesports.io to the new latam sports platform 

# focus for this is competition, teams, and logos for these 

import requests
import json 
import os
import xmltodict
from pathlib import Path

TEAM_IMAGE_URL = 'http://images.firesport.io/cdf/team/'
TEAM_API = 'https://api.cdf.firesport.io/team/'
COMP_API = 'https://api.cdf.firesport.io/championship/'
COMP_IMAGE_URL = 'http://images.firesport.io/o/cdf/championship/'
HIGH = 'https://api.cdf.firesport.io/video/?limit=20&ntype=COMPLETE_MATCH&match='
PROG = 'https://api.cdf.firesport.io/programcategory/'
PROG_DETAILS = 'https://api.cdf.firesport.io/video/?limit=10&program='
PROD_DET = 'https://api.cdf.firesport.io/program/'


payload = {}
genre = {
    'eng':{'Actualidad': 'News', 'Docu-Series': 'Documentary', 'Entrevistas': 'Interview', 'Programas de Fútbol': 'Football', 'Otros': 'Other'},
    'spa': {'Actualidad': 'Noticias','Docu-Series': 'Documental', 'Entrevistas': 'Entrevistas', 'Programas de Fútbol': 'Fútbol', 'Otros': 'Otros'},
    'por': {'Actualidad': 'Noticias', 'Docu-Series': 'Documentário', 'Entrevistas': 'Entrevistas', 'Programas de Fútbol': 'Futebol', 'Otros': 'Otros'}
}

headers = {
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI2MTViZTc5NC02Mzk3LTRkZGYtODE3ZS03NDY3ZDljMTAwMWEiLCJ1c2VySWQiOiI1Y2VkNjc2NjMzNzJkZTM3ZmMxZjJiMzUiLCJub21icmUiOiJHdWVzdCIsImFwZWxsaWRvUGF0ZXJubyI6IlR1cm5lciIsImZvdG8iOiIiLCJmZWNoYV9uYWNpbWllbnRvIjoiMTkwMC0wMS0wMSIsImZlY2hhTmFjaW1pZW50byI6IjE5MDAtMDEtMDEiLCJnZW5lcm8iOiJmZW1lbmlubyIsImVtYWlsIjoidHVybmVyZ3Vlc3RfMDFAdHVybmVyLmNvbSIsImVzdGFkbyI6ImNvbXBsZXRhZG8iLCJwYXNhcmVsYSI6ImN1cG9uIiwibWF0ZXJpYWwiOiJGdWxsIiwiZGlzcG9zaXRpdm9zIjoiMSIsImlzcyI6ImFwaS5jZGYuY2wiLCJhcHBJZCI6ImVjZGYiLCJyZW1lbWJlciI6dHJ1ZSwiaWF0IjoxNTg4ODYyMjUwLCJleHAiOjE1ODg5NDg2NTB9.KBKy0ioWiBpY-8KM0pbHQuGRQ40cYQhTHv7ARfY-zu4u7mrqAWPf6EXQarkndE57qOoUeeiDL4IWBcqfIKkcGooYT9jUkKnS8CF1lzBVAZTB_lPk3N7Vt-ZmdsaQSFvHkDU7ysyn93Z5reDMKZI-Y7Va3m-_2kbpZGd3Psd8iz6emqXoft-r0SMOavl7Ph1_ho_ad0DAnI7JFkceWwClF0WTLhMkwbhYFbUQKChEX5LeUT0WVIRvAUHRu789APx1YQ2AOtFyGWXUV5HiDJlfnZiN1sONg57ykjDYuSOT311ebpCc1mect4uuxvrGtcC8ndINw1xSc83vaxx-oDAwyQ'
}

def write_img(id, type2):
    if type2 == 'team':
        try:
            img_data = requests.get(TEAM_IMAGE_URL + str(id) + '/image.png').content
            with open( 'team/'+str(id)+'/'+'image.png', 'wb') as handler:
                handler.write(img_data)
        except:
            print('image error')

    else : 
        img_data = requests.get(COMP_IMAGE_URL + str(id) + '/logo/web.png').content
        with open('competition/'+str(id)+'/'+'image.png', 'wb') as handler:
            handler.write(img_data)

def write_json(team):
    team2 = {}
    team2['id'] = team['id']
    team2['name'] = team['name']
    team2['sport'] = 'Futbol'
    team2['colors'] = ['0x343234','0x3234234']
    team2['logos'] = TEAM_IMAGE_URL + str(team['id']) + '/image.png'
    team2['sponsors'] = [{'name' : 'coca-cola', 'logo' : 'image_url'}]

    with open( 'team/'+str(team['id'])+'.json', 'w') as outfile:
        json.dump(team2, outfile)


def create_bundle(match, round, round_dir):
    match_title = match['localTeam']['name'] + \
        ' vs ' + match['visitorTeam']['name']
    team1_id = match['localTeam']['id']
    team2_id = match['visitorTeam']['id']
    team1_name = match['localTeam']['name']
    team2_name = match['visitorTeam']['name']
    team1 = 'TEAM_Home_' +  team1_id + '_' + team1_name
    team2 = 'TEAM_Away_' + team2_id + '_' + team2_name
    with open('templates/SPORTS_LATAM_CDF_xxxx_BUNDLE_ADD.xml') as fd:
        doc = xmltodict.parse(fd.read())

    doc['BundleGroup']['Bundle']['HouseId'] = match['id']
    doc['BundleGroup']['Bundle']['Title'] = match_title
    doc['BundleGroup']['Bundle']['UITitle'] = match_title
    doc['BundleGroup']['Bundle']['SummaryShort'] = 'Round ' + round
    doc['BundleGroup']['Bundle']['SummaryLong'] = 'Round' + round



    doc['BundleGroup']['Bundle']['SearchKeywordList']['Keyword'][0] = team1
    doc['BundleGroup']['Bundle']['SearchKeywordList']['Keyword'][1] = team2

    doc['BundleGroup']['Bundle']['MultilangMetadataList']['MultilangMetadata'][0]['Title'] = match_title
    doc['BundleGroup']['Bundle']['MultilangMetadataList']['MultilangMetadata'][1]['Title'] = match_title

    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][3]['Value'] = 'Round ' + round 
    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][9]['Value'] = 'Fecha ' + round
    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][15]['Value'] = 'Fecha ' + round


    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][4]['Value'] = match['calculatedDate']
    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][5]['Value'] = match['calculatedDate']
    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][10]['Value'] = match['calculatedDate']
    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][11]['Value'] = match['calculatedDate']
    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][16]['Value'] = match['calculatedDate']
    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][17]['Value'] = match['calculatedDate']

    tm = ['3_1_Hero', '16_9_Hero', '4_3_Hero', '1_1_Badge', '16_9_Tile', '1_1_Tile']
    for num, image in enumerate(doc['BundleGroup']['Bundle']['Extensions']['AssetList']['AssetFile'],start=0):
        image['ExternalAssetId'] = match['id'] + '_' + tm[num]

    
    out = xmltodict.unparse(doc, pretty=True)
    with open(round_dir + "/SPORTS_LATAM_CDF_" + match['id'] + "_BUNDLE_ADD.xml", 'a') as file:
        file.write(out)
    
    if match['vod']:
        with open('templates/SPORTS_LATAM_CDF_xxxx_EVENT_REPLAY_EPISODE_ADD.xml') as fd2:
            doc2 = xmltodict.parse(fd2.read())

        doc2['Asset']['HouseId'] = match['id'] + '01'
        doc2['Asset']['Title'] = match_title
        doc2['Asset']['TitleBrief'] = match_title
        doc2['Asset']['ParentObjectList']['ParentObjectId'] = match['id']
        doc2['Asset']['SearchKeywordList']['Keyword'][0] = team1
        doc2['Asset']['SearchKeywordList']['Keyword'][1] = team2

        doc2['Asset']['MultilangMetadataList']['MultilangMetadata'][0]['Title'] = match_title
        doc2['Asset']['MultilangMetadataList']['MultilangMetadata'][1]['Title'] = match_title
        doc2['Asset']['EMFAttributeList']['EMFAttribute'][0]['Value'] = match['id']
        doc2['Asset']['EMFAttributeList']['EMFAttribute'][5]['Value'] = match['id']
        doc2['Asset']['EMFAttributeList']['EMFAttribute'][10]['Value'] = match['id']
        
        doc2['Asset']['EMFAttributeList']['EMFAttribute'][4]['Value'] = match['calculatedDate']
        doc2['Asset']['EMFAttributeList']['EMFAttribute'][9]['Value'] = match['calculatedDate']
        doc2['Asset']['EMFAttributeList']['EMFAttribute'][14]['Value'] = match['calculatedDate']

        for platform in doc2['Asset']['Extensions']['PlatformList']['Platform']:
            platform['VideoUrl'] = match['vodData']['url']
            platform['PosterAsset'] = match['id'] + '01'

        out2 = xmltodict.unparse(doc2, pretty=True)
        with open(round_dir + "/SPORTS_LATAM_CDF_" + match['id'] + '01' + "_EVENT_REPLAY_EPISODE_ADD.xml", 'a') as file2:
            file2.write(out2)
        
        url = HIGH + str(match['id'])

        response = requests.request("GET", url, headers=headers, data=payload)
        data2 = response.json()
        
        for i, stream in enumerate(data2, start=2) :
            if stream['type'] == 'COMPACT_SECOND_HALF' or stream['type'] == 'COMPACT_FIRST_HALF':
                gen(doc2, match, stream, 'HIGHLIGHT', i, round_dir)

            elif stream['type'] == 'SCORE' :
                gen(doc2, match, stream, 'GOAL', i, round_dir)
                
    else:
        with open('templates/SPORTS_LATAM_CDF_xxxx_LIVE_EVENT_EPISODE_ADD.xml') as fd3:
            doc3 = xmltodict.parse(fd3.read())
        try:
            match_start = match['event']['start']
            match_end = match['event']['end']
            event_id = str(match['event']['id'])
        except:
            match_start = match['calculatedDate']
            match_end = match['calculatedDate']
            event_id = '_pending_'
        doc3['Asset']['HouseId'] = match['id'] + event_id
        
        doc3['Asset']['Title'] = match_title
        doc3['Asset']['TitleBrief'] = match_title
        doc3['Asset']['ParentObjectList']['ParentObjectId'] = match['id']
        doc3['Asset']['SearchKeywordList']['Keyword'][0] = team1
        doc3['Asset']['SearchKeywordList']['Keyword'][1] = team2

        doc3['Asset']['MultilangMetadataList']['MultilangMetadata'][0]['Title'] = match_title
        doc3['Asset']['MultilangMetadataList']['MultilangMetadata'][1]['Title'] = match_title
        doc3['Asset']['EMFAttributeList']['EMFAttribute'][0]['Value'] = match['id']
        doc3['Asset']['EMFAttributeList']['EMFAttribute'][4]['Value'] = match['id']
        doc3['Asset']['EMFAttributeList']['EMFAttribute'][8]['Value'] = match['id']
        
        doc3['Asset']['Properties']['Property']['StreamStartDate'] = match_start
        doc3['Asset']['Properties']['Property']['StreamEndDate'] = match_end

        for platform in doc3['Asset']['Extensions']['PlatformList']['Platform']:
            try:
                platform['VideoUrl'] = match['event']['recording']['apps'][0]['stream']['streamUrl']
                platform['PosterAsset'] = match['id'] + event_id

            except:
                platform['VideoUrl'] = "https://no-url"
                platform['PosterAsset'] = match['id'] + event_id

        
        out3 = xmltodict.unparse(doc3, pretty=True)
        with open(round_dir + "/SPORTS_LATAM_CDF_" + match['id'] + event_id + "_LIVE_EVENT_EPISODE_ADD.xml", 'a') as file2:
            file2.write(out3)       

def gen(doc,match,stream,type, i, round_dir):
    doc['Asset']['HouseId'] = match['id'] + str(i)
    doc['Asset']['Title'] = stream['title']
    doc['Asset']['TitleBrief'] = stream['title']
    doc['Asset']['ObjectSubtype'] = type
    for platform in doc['Asset']['Extensions']['PlatformList']['Platform']:
        platform['VideoUrl'] = stream['url']
        platform['PosterAsset'] = match['id'] + str(i)
    out = xmltodict.unparse(doc, pretty=True)
    with open(round_dir + "/SPORTS_LATAM_CDF_" + match['id'] + str(i) + "_"+type+"_EPISODE_ADD.xml", 'a') as file:
        file.write(out)

def games():
    for i in range(1, 34):
        os.mkdir('round_' + str(i))
        url = 'https://api.cdf.firesport.io/championship/722-2020/footballdate/722-2020-1-' + \
            str(i) + '?include=event,video'

        response = requests.request("GET", url, headers=headers, data=payload)
        print(response)
        data = response.json()
        for match in data:
            create_bundle(match, str(i), 'round_'+str(i))

def create_gob(program,cat, dir):
    with open('templates/SPORTS_LATAM_CDF_xx_GOB_ADD.xml') as fd:
        doc = xmltodict.parse(fd.read())
    pr_id = str(program['id'])
    doc['GroupOfBundleGroup']['GroupOfBundle']['HouseId'] = pr_id
    doc['GroupOfBundleGroup']['GroupOfBundle']['Title'] = program['name']
    doc['GroupOfBundleGroup']['GroupOfBundle']['UITitle'] = program['name']
    doc['GroupOfBundleGroup']['GroupOfBundle']['SummaryShort'] = program['description']
    doc['GroupOfBundleGroup']['GroupOfBundle']['SummaryLong'] = program['description']
    doc['GroupOfBundleGroup']['GroupOfBundle']['GenreList']['GenreItem'] = genre['eng'][cat]

    doc['GroupOfBundleGroup']['GroupOfBundle']['MultilangMetadataList']['MultilangMetadata'][0]['Title'] = program['name']
    doc['GroupOfBundleGroup']['GroupOfBundle']['MultilangMetadataList']['MultilangMetadata'][0]['ShortDescription'] = program['description']
    doc['GroupOfBundleGroup']['GroupOfBundle']['MultilangMetadataList']['MultilangMetadata'][0]['LongDescription'] = program['description']
    doc['GroupOfBundleGroup']['GroupOfBundle']['MultilangMetadataList']['MultilangMetadata'][0]['GenreList']['GenreItem'] = genre['spa'][cat]

    doc['GroupOfBundleGroup']['GroupOfBundle']['MultilangMetadataList']['MultilangMetadata'][1]['Title'] = program['name']
    doc['GroupOfBundleGroup']['GroupOfBundle']['MultilangMetadataList']['MultilangMetadata'][1]['ShortDescription'] = program['description']
    doc['GroupOfBundleGroup']['GroupOfBundle']['MultilangMetadataList']['MultilangMetadata'][1]['LongDescription'] = program['description']
    doc['GroupOfBundleGroup']['GroupOfBundle']['MultilangMetadataList']['MultilangMetadata'][1]['GenreList']['GenreItem'] = genre['por'][cat]

    doc['GroupOfBundleGroup']['GroupOfBundle']['Extensions']['AssetList']['AssetFile'][0]['ExternalAssetId'] = pr_id + '_16_9_Hero'
    doc['GroupOfBundleGroup']['GroupOfBundle']['Extensions']['AssetList']['AssetFile'][1]['ExternalAssetId'] = pr_id + '_4_3_Hero'
    doc['GroupOfBundleGroup']['GroupOfBundle']['Extensions']['AssetList']['AssetFile'][2]['ExternalAssetId'] = pr_id + '_3_1_Hero'
    doc['GroupOfBundleGroup']['GroupOfBundle']['Extensions']['AssetList']['AssetFile'][3]['ExternalAssetId'] = pr_id + '_16_9_Tile'
    for platform in doc['GroupOfBundleGroup']['GroupOfBundle']['Extensions']['PlatformList']['Platform']:
        platform['PosterAsset'] = pr_id

    out = xmltodict.unparse(doc, pretty=True)
    with open(dir + "SPORTS_LATAM_CDF_" + pr_id +"_GOB_ADD.xml", 'a') as file:
        file.write(out)


def create_bundle2(program, cat, dir, bundle):
    with open('templates/SPORTS_LATAM_CDF_xxyy_Bundle_ADD.xml') as fd:
        doc = xmltodict.parse(fd.read())
    pr_id = str(program['id']) + bundle
    doc['BundleGroup']['Bundle']['HouseId'] = pr_id
    doc['BundleGroup']['Bundle']['Title'] = program['name']
    doc['BundleGroup']['Bundle']['UITitle'] = program['name']
    doc['BundleGroup']['Bundle']['SummaryShort'] = program['description']
    doc['BundleGroup']['Bundle']['SummaryLong'] = program['description']
    doc['BundleGroup']['Bundle']['GenreList']['GenreItem'] = genre['eng'][cat]
    doc['BundleGroup']['Bundle']['ParentObjectList']['ParentObjectId'] = str(
        program['id'])

    doc['BundleGroup']['Bundle']['MultilangMetadataList']['MultilangMetadata'][0]['Title'] = program['name']
    doc['BundleGroup']['Bundle']['MultilangMetadataList']['MultilangMetadata'][0]['ShortDescription'] = program['description']
    doc['BundleGroup']['Bundle']['MultilangMetadataList']['MultilangMetadata'][0]['LongDescription'] = program['description']
    doc['BundleGroup']['Bundle']['MultilangMetadataList']['MultilangMetadata'][0]['GenreList']['GenreItem'] = genre['spa'][cat]

    doc['BundleGroup']['Bundle']['MultilangMetadataList']['MultilangMetadata'][1]['Title'] = program['name']
    doc['BundleGroup']['Bundle']['MultilangMetadataList']['MultilangMetadata'][1]['ShortDescription'] = program['description']
    doc['BundleGroup']['Bundle']['MultilangMetadataList']['MultilangMetadata'][1]['LongDescription'] = program['description']
    doc['BundleGroup']['Bundle']['MultilangMetadataList']['MultilangMetadata'][1]['GenreList']['GenreItem'] = genre['por'][cat]

   
    for platform in doc['BundleGroup']['Bundle']['Extensions']['PlatformList']['Platform']:
        platform['PosterAsset'] = pr_id

    out = xmltodict.unparse(doc, pretty=True)
    with open(dir + "SPORTS_LATAM_CDF_" + pr_id + "_BUNDLE_ADD.xml", 'a') as file:
        file.write(out)

def create_program(program, cat, dir):
    url = PROG_DETAILS + str(program['id']) + '&type=PROGRAM'
    url2 = PROD_DET + str(program['id'])
    response = requests.request(
        "GET", url, headers=headers, data=payload)
    data = response.json()

    response2 = requests.request(
        "GET", url2, headers=headers, data=payload)
    data2 = response2.json()
    description = data2['description']
    print(program['name'])
    
    for i, prog in enumerate(data, start=1):
        pr_id = str(program['id']) + '00' + str(i)
        with open('templates/SPORTS_LATAM_CDF_xxyyzz_EPISODE_ADD.xml') as fd2:
            doc = xmltodict.parse(fd2.read())
        pr_name = program['name'] + ' ' + prog['exportAt']

        doc['Asset']['HouseId'] = pr_id
        doc['Asset']['Title'] = pr_name
        doc['Asset']['TitleBrief'] = pr_name
        doc['Asset']['Season'] = '0'
        doc['Asset']['EpisodeId'] = str(i)

        doc['Asset']['ParentObjectList']['ParentObjectId'] = str(
            program['id']) + '00'

        doc['Asset']['MultilangMetadataList']['MultilangMetadata'][0]['Title'] = pr_name
        doc['Asset']['MultilangMetadataList']['MultilangMetadata'][0]['ShortDescription'] = description
        doc['Asset']['MultilangMetadataList']['MultilangMetadata'][0]['LongDescription'] = description
        doc['Asset']['MultilangMetadataList']['MultilangMetadata'][0]['GenreList']['GenreItem'] = genre['spa'][cat]

        doc['Asset']['MultilangMetadataList']['MultilangMetadata'][1]['Title'] = pr_name
        doc['Asset']['MultilangMetadataList']['MultilangMetadata'][1]['ShortDescription'] = description
        doc['Asset']['MultilangMetadataList']['MultilangMetadata'][1]['LongDescription'] = description
        doc['Asset']['MultilangMetadataList']['MultilangMetadata'][1]['GenreList']['GenreItem'] = genre['spa'][cat]
        
        for platform in doc['Asset']['Extensions']['PlatformList']['Platform']:
            platform['VideoUrl'] = prog['url']
            platform['PosterAsset'] = pr_id

        out = xmltodict.unparse(doc, pretty=True)
        with open(dir + "/SPORTS_LATAM_CDF_" + pr_id + "_EPISODE_ADD.xml", 'a') as file2:
            file2.write(out)


def main():
    print('start')
    response = requests.request("GET", PROG, headers=headers, data=payload)
    data = response.json()
    for category in data :
        print(category['name'])
        Path(category['name']).mkdir(parents=True, exist_ok=True)
        for program in category['programs']:
            Path(category['name']+'/'+program['name']).mkdir(parents=True, exist_ok=True)
            
            create_gob(program, category['name'], category['name']+'/'+program['name']+'/')
            create_bundle2(program, category['name'],
                       category['name']+'/'+program['name']+'/', '00')

            create_program( program, category['name'], category['name']+'/'+program['name']+'/')



  

if __name__ == '__main__': 
    main()
