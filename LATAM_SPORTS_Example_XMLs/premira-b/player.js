function convertTime(millis) {
    if (!millis) return null;
    var dateParsed = new Date(millis),
        date = dateParsed.getUTCDate() < 10 ? "0" + dateParsed.getUTCDate() : dateParsed.getUTCDate(),
        month = dateParsed.getUTCMonth() + 1 < 10 ? "0" + (dateParsed.getUTCMonth() + 1) : dateParsed.getUTCMonth() + 1,
        year = dateParsed.getUTCFullYear(),
        hours = dateParsed.getUTCHours() < 10 ? "0" + dateParsed.getUTCHours() : dateParsed.getUTCHours(),
        minutes = dateParsed.getUTCMinutes() < 10 ? "0" + dateParsed.getUTCMinutes() : dateParsed.getUTCMinutes(),
        seconds = dateParsed.getUTCSeconds() < 10 ? "0" + dateParsed.getUTCSeconds() : dateParsed.getUTCSeconds(),
        milliseconds = dateParsed.getUTCMilliseconds() < 10 ? "00" + dateParsed.getUTCMilliseconds() : dateParsed.getUTCMilliseconds() < 100 ? "0" + dateParsed.getUTCMilliseconds() : dateParsed.getUTCMilliseconds();
    return year + "-" + month + "-" + date + "T" + hours + ":" + minutes + ":" + seconds + "." + milliseconds + "Z"
}

function createErrorDataContainer(errSrc, errCode, errMsg) {
    var bo = BOContentFactory.getInstance(),
        cc = bo.getCurrentContent(),
        MM = MMFactory.getInstance();
    return {
        domain: window.location.hostname,
        platform: "PCTV",
        propertyName: "VIDEOPLAY_EMEA_ZA",
        contentId: MM.getStats().media.contentId,
        networkType: "WIFI",
        bitRate: 0,
        eventType: "error",
        contentType: window.contentType,
        genre: cc.genre,
        contractStartDate: convertTime(cc.contractStart),
        contractEndDate: convertTime(cc.contractEnd),
        errSrc: errSrc || "",
        errCode: errCode || "",
        errMsg: errMsg || ""
    }
}

function saveLastPlayedContentData() {
    var playerMetrics = BitmovinPlayerBridge.getMetrics(),
        MM = MMFactory.getInstance(),
        videoDataContainer = {
            contentId: MM.getStats().media.contentId,
            contentType: MM.getStats().media.contentType,
            bookmark: playerMetrics.bookmark || 0,
            deltaThreshold: playerMetrics.deltaThreshold || 0
        },
        stringified = JSON.stringify(videoDataContainer);
    localStorage.setItem("videoDataContainer", stringified)
}

function saveLastErrorData(errorData) {
    var stringified = JSON.stringify(errorData);
    localStorage.setItem("videoErrorData", stringified)
}

function createBitmovinPlayerBridge() {
    function round(number, precision) {
        var shift = function (number, precision) {
            var numArray = ("" + number).split("e");
            return +(numArray[0] + "e" + (numArray[1] ? +numArray[1] + precision : precision))
        };
        return shift(Math.round(shift(number, +precision)), -precision)
    }

    function resizeEvent() {
        null !== document.getElementById(this.videoDOMElementID) && BitmovinPlayerBridge.videoDOMElement.setAttribute("style", BitmovinPlayerBridge.calcPlayerSize())
    }

    function hashChangeEvent() {
        BitmovinPlayerBridge.playerInstance && BitmovinPlayerBridge.playerInstance.getVideoQuality() && MM.stop()
    }

    function getCookie(cn) {
        for (var name = cn + "=", allCookie = decodeURIComponent(document.cookie).split(";"), cval = [], i = 0; i < allCookie.length; i++) 0 == allCookie[i].trim().indexOf(name) && (cval = allCookie[i].trim().split("="));
        return cval.length > 0 ? cval[1] : ""
    }
    var BitmovinPlayerBridge = null,
        MM = MMFactory.getInstance(),
        deltaThreshold = {
            value: 0,
            isForward: !1,
            _startPlayTime: 0,
            _stopPlayTime: 0,
            _isUpdated: !1,
            getCurrentValue: function () {
                return BitmovinPlayerBridge && BitmovinPlayerBridge.playerInstance && BitmovinPlayerBridge.playerInstance.getCurrentTime && this._startPlayTime ? this.value + BitmovinPlayerBridge.playerInstance.getCurrentTime() - this._startPlayTime : 0
            },
            update: function () {
                this.isForward = !1, this._isUpdated || (this.value += this._stopPlayTime >= this._startPlayTime ? this._stopPlayTime - this._startPlayTime : 0, this._startPlayTime = 0, this._isUpdated = !0)
            },
            setPlayTime: function (time) {
                this.isForward || (this._startPlayTime = time, this._isUpdated = !1)
            },
            setStopTime: function (time) {
                var that = this;
                setTimeout(function () {
                    that._stopPlayTime = BitmovinPlayerBridge.playerInstance ? time : 0
                }, 100)
            },
            clear: function () {
                this.value = 0, this._startPlayTime = 0, this._stopPlayTime = 0, this._isUpdated = !1, this.isForward = !1
            }
        };
    return BitmovinPlayerBridge = {
        videoDOMElement: null,
        playerInstance: null,
        player: null,
        playerUiManager: null,
        contentParams: null,
        mediaEventsManager: null,
        videoDOMElementID: "videoPlayer",
        play: function (contentParams, mediaEventsManager, callback) {
            deltaThreshold.clear(), this.contentParams = contentParams, this.mediaEventsManager = mediaEventsManager, this.contentParams.height = parseInt(this.contentParams.height), this.createPlayerDOMNode(), this.attachResizeListener(), this.startPlayback(callback), playerBufferingStartTS = 0, playerBufferingEndTS = 0, playerBufferingTime = 0
        },
        pause: function (callback) {
            this.pauseVideo(callback)
        },
        resume: function (callback) {
            this.resumeVideo(callback)
        },
        seek: function (seconds, callback) {
            this.seekProgress(seconds, callback)
        },
        stop: function (callback) {
            deltaThreshold.update(), this.destroyPlayer(callback)
        },
        getMetrics: function () {
            return this.getMetricsData()
        },
        attachResizeListener: function () {
            window.addEventListener("resize", resizeEvent)
        },
        createPlayerDOMNode: function () {
            BitmovinPlayerBridge.videoDOMElement = document.createElement("div"), BitmovinPlayerBridge.videoDOMElement.setAttribute("id", BitmovinPlayerBridge.videoDOMElementID), BitmovinPlayerBridge.videoDOMElement.setAttribute("style", BitmovinPlayerBridge.calcPlayerSize()), BitmovinPlayerBridge.contentParams.parentView.appendChild(BitmovinPlayerBridge.videoDOMElement)
        },
        startPlayback: function (callback) {
            var _this = this,
                conf = {
                    key: "4d2865e0-6ab6-46f4-9217-ac26e9c75dc9",
                    playback: {
                        autoplay: !0
                    },
                    ui: !1,
                    events: {
                        downloadfinished: function (data) {
                            if ("media/video" === data.downloadType) var availableVideoQualities = availableVideoQualities || _this.player.getAvailableVideoQualities()
                        },
                        error: function (details) {}
                    }
                },
                source = {};
            if ("LIVE_CHANNEL" === MM.getStats().media.contentInfo.objectSubtype.toUpperCase()) source = {
                hls: _this.contentParams.urlManifest,
                options: {
                    hlsWithCredentials: !0,
                    manifestWithCredentials: !0,
                    withCredentials: !0
                }
            };
            else {
                var DRMtoken = localStorage.getItem("getDRMTokenAGL");
                source = {
                    dash: _this.contentParams.urlManifest,
                    drm: {
                        widevine: {
                            LA_URL: "https://widevine.license.istreamplanet.com/widevine/api/license/7837c2c6-8fe4-4db0-9900-1bd66c21ffa3",
                            headers: {
                                "x-isp-token": DRMtoken
                            }
                        }
                    }
                }
            }
            var playerContainer = document.getElementById(this.videoDOMElementID);
            _this.player = new bitmovin.player.Player(playerContainer, conf);
            var userAuthData = JSON.parse(getCookie("rf_data"));
            "undefined" != typeof youbora && youbora.adapters.Bitmovin8 && (window.plugin = new youbora.Plugin({
                accountCode: "turnerlatam"
            }), plugin.setOptions({
                username: userAuthData.mvpd,
                "content.title": MM.getStats().media.contentInfo.additionalData.TitoloUnicoUI,
                "content.resource": _this.contentParams.urlManifest,
                "content.isLive": "LIVE_CHANNEL" === MM.getStats().media.contentInfo.objectSubtype.toUpperCase(),
                "extraparam.1": "CDF"
            }), plugin.setAdapter(new youbora.adapters.Bitmovin8(_this.player))), this.mediaEventsManager.receive({
                name: "PLAY"
            });
            var beforeBrowserClose = function () {
                BitmovinPlayerBridge && BitmovinPlayerBridge.playerInstance && BitmovinPlayerBridge.playerInstance.getVideoQuality() && (round(BitmovinPlayerBridge.playerInstance.getCurrentTime(), 0), MM.directStopContent())
            };
            "Chrome" === deviceData.browser ? AVSJQ(window).on("beforeunload", beforeBrowserClose) : AVSJQ(window).on("unload", beforeBrowserClose);
            var customAllErrorMessageOverlayConfig = function (error) {
                    return "Algo salió mal. Por favor intenta luego. Código de error: ".concat(error.code).toUpperCase()
                },
                uiConfig = {
                    errorMessages: customAllErrorMessageOverlayConfig
                };
            _this.playerUiManager = bitmovin.playerui.UIFactory.buildDefaultUI(_this.player, uiConfig), _this.player.on(bitmovin.player.PlayerEvent.VideoQualityChanged, function (e) {
                "undefined" != typeof youbora && youbora.adapters.Bitmovin8 && plugin.setOptions({
                    "content.bitrate": e.targetQuality.bitrate,
                    "content.encoding.videoCodec": e.targetQuality.codec
                })
            }), _this.player.load(source).then(function (player) {}, function (reason) {}), callback(!0)
        },
        calcPlayerSize: function () {
            var pWidth, pHeight, width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
                height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
            return width >= this.contentParams.width && height >= this.contentParams.height && (pWidth = this.contentParams.width, pHeight = this.contentParams.height), height < this.contentParams.height && (width < 1.7 * this.contentParams.height ? (pWidth = width, pHeight = width / 1.7) : (pHeight = height, pWidth = 1.7 * height)), width < this.contentParams.width && (height < this.contentParams.width / 1.7 ? (pHeight = height, pWidth = 1.7 * height) : (pWidth = width, pHeight = width / 1.7)), "width:" + pWidth + "px; height:" + pHeight + "px; bottom: 0; margin-top: 0"
        },
        pauseVideo: function (callback) {
            this.playerInstance && (this.playerInstance.pause(), callback(!0))
        },
        resumeVideo: function (callback) {
            this.playerInstance && (this.playerInstance.resume(), callback(!0))
        },
        seekProgress: function (seconds, callback) {
            this.playerInstance && seconds >= 0 && (this.playerInstance.seek(seconds), callback(!0))
        },
        getMetricsData: function () {
            var progressTime = this.playerInstance ? Math.round(this.playerInstance.getLiveCurrentTime()) : 0,
                videoQuality = this.playerInstance ? this.playerInstance.getVideoPlaybackQuality() : {
                    corruptedVideoFrames: 0,
                    creationTime: Date.now(),
                    droppedVideoFrames: 0,
                    totalFrameDelay: 0,
                    totalVideoFrames: 0
                };
            return this.playerInstance && this.playerInstance.htmlVideoElement.duration <= progressTime && (progressTime = 0), {
                bookmark: progressTime || 0,
                deltaThreshold: Math.round(deltaThreshold._isUpdated ? deltaThreshold.value : deltaThreshold.getCurrentValue()),
                droppedVideoFrames: videoQuality.droppedVideoFrames
            }
        },
        destroyPlayer: function (callback) {
            window.removeEventListener("resize", resizeEvent), window.removeEventListener("resize", hashChangeEvent), this.playerUiManager.release(), null !== this.player && ("undefined" != typeof youbora && plugin.removeAdapter(), this.player.destroy()), this.playerInstance = null, this.videoDOMElement = null, callback(!0)
        }
    }
}
var BitmovinPlayerBridge = createBitmovinPlayerBridge(),
    playerBufferingStartTS, playerBufferingEndTS, playerBufferingTime, deviceData = (new DeviceUUID).parse();
window.disableSDKManifestFetch = !0;