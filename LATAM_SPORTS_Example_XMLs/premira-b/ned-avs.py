# A script of migrating data from firesports.io to the new latam sports platform 

# focus for this is competition, teams, and logos for these 

import requests
import json 
import os
import xmltodict
from pathlib import Path

TEAM_IMAGE_URL = 'http://images.firesport.io/cdf/team/'
TEAM_API = 'https://api.cdf.firesport.io/team/'
COMP_API = 'https://api.cdf.firesport.io/championship/'
COMP_IMAGE_URL = 'http://images.firesport.io/o/cdf/championship/'
HIGH = 'https://api.cdf.firesport.io/video/?limit=20&ntype=COMPLETE_MATCH&match='
PROG = 'https://api.cdf.firesport.io/programcategory/'
PROG_DETAILS = 'https://api.cdf.firesport.io/video/?limit=30&program=95&type=PROGRAM'

payload = {}


headers = {
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJqdGkiOiJjNTg4NjFmOS1hYjQ4LTQxNjItODhkNy0zZjQxOWNiMDAyZjciLCJ1c2VySWQiOiI1ZGYzOTczYmJlNDE5Nzc2ZTI5OWE1ZTkiLCJub21icmUiOiJUdXJuZXIiLCJhcGVsbGlkb1BhdGVybm8iOiJHdWVzdCIsImZvdG8iOiIiLCJmZWNoYV9uYWNpbWllbnRvIjoiMTkwMC0wMS0wMSIsImZlY2hhTmFjaW1pZW50byI6IjE5MDAtMDEtMDEiLCJnZW5lcm8iOiIiLCJlbWFpbCI6InR1cm5lcmd1ZXN0XzA0QHR1cm5lci5jb20iLCJlc3RhZG8iOiJjb21wbGV0YWRvIiwicGFzYXJlbGEiOiJjdXBvbiIsIm1hdGVyaWFsIjoiRnVsbCIsImRpc3Bvc2l0aXZvcyI6IjEiLCJpc3MiOiJhcGkuY2RmLmNsIiwiYXBwSWQiOiJlY2RmIiwicmVtZW1iZXIiOnRydWUsImlhdCI6MTU4OTQwMTY4OCwiZXhwIjoxNTg5NDg4MDg4fQ.crgBrivmO8QqdxXRUDrTIPV98bovW27sUzQYbi4nFxamkNCCwg3Zyyqh0t_-qXAjFonb_SAx6YO3ZcsClznI6Z783TzEk2f1UaWC_TxOR5GBPXvIeI5lxhHhqQmLw_on8Q5hfiMqlCNF_VsR1YyYNXAnutiSsAbtenMPQV5BOxBvP6WoejDoq8O4uAi3tltuDLwDrU15NtAtMyd3u3UZnzhcCarJDLe1A6xPARjZb7jv9QgfYf_6D9u8OTKsvA4Sk-cZB9lU4bMGQBv0f-ONyhiCf28yhyfYgLOWefUL-_N2ZEcv7IJhjlUj0y0Vx4zJNLV3cJXpAtlukgp_hf6xMQ'
}

def write_img(id, type2):
    if type2 == 'team':
        try:
            img_data = requests.get(TEAM_IMAGE_URL + str(id) + '/image.png').content
            with open( 'team/'+str(id)+'/'+'image.png', 'wb') as handler:
                handler.write(img_data)
        except:
            print('image error')

    else : 
        img_data = requests.get(COMP_IMAGE_URL + str(id) + '/logo/web.png').content
        with open('competition/'+str(id)+'/'+'image.png', 'wb') as handler:
            handler.write(img_data)

def write_json(team):
    team2 = {}
    team2['id'] = team['id']
    team2['name'] = team['name']
    team2['sport'] = 'Futbol'
    team2['colors'] = ['0x343234','0x3234234']
    team2['logos'] = TEAM_IMAGE_URL + str(team['id']) + '/image.png'
    team2['sponsors'] = [{'name' : 'coca-cola', 'logo' : 'image_url'}]

    with open( 'team/'+str(team['id'])+'.json', 'w') as outfile:
        json.dump(team2, outfile)


def create_bundle(match, round, round_dir):
    match_title = match['localTeam']['name'] + \
        ' vs ' + match['visitorTeam']['name']
    team1_id = match['localTeam']['id']
    team2_id = match['visitorTeam']['id']
    team1_name = match['localTeam']['name']
    team2_name = match['visitorTeam']['name']
    team1 = 'TEAM_Home_' +  team1_id + '_' + team1_name
    team2 = 'TEAM_Away_' + team2_id + '_' + team2_name
    with open('templates/SPORTS_LATAM_CDF_xxxx_BUNDLE_ADD.xml') as fd:
        doc = xmltodict.parse(fd.read())

    doc['BundleGroup']['Bundle']['HouseId'] = match['id']
    doc['BundleGroup']['Bundle']['Title'] = match_title
    doc['BundleGroup']['Bundle']['UITitle'] = match_title
    doc['BundleGroup']['Bundle']['SummaryShort'] = 'Round ' + round
    doc['BundleGroup']['Bundle']['SummaryLong'] = 'Round' + round



    doc['BundleGroup']['Bundle']['SearchKeywordList']['Keyword'][0] = team1
    doc['BundleGroup']['Bundle']['SearchKeywordList']['Keyword'][1] = team2

    doc['BundleGroup']['Bundle']['MultilangMetadataList']['MultilangMetadata'][0]['Title'] = match_title
    doc['BundleGroup']['Bundle']['MultilangMetadataList']['MultilangMetadata'][1]['Title'] = match_title

    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][3]['Value'] = 'Round ' + round 
    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][9]['Value'] = 'Fecha ' + round
    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][15]['Value'] = 'Fecha ' + round


    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][4]['Value'] = match['calculatedDate']
    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][5]['Value'] = match['calculatedDate']
    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][10]['Value'] = match['calculatedDate']
    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][11]['Value'] = match['calculatedDate']
    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][16]['Value'] = match['calculatedDate']
    doc['BundleGroup']['Bundle']['EMFAttributeList']['EMFAttribute'][17]['Value'] = match['calculatedDate']

    tm = ['3_1_Hero', '16_9_Hero', '4_3_Hero', '1_1_Badge', '16_9_Tile', '1_1_Tile']
    for num, image in enumerate(doc['BundleGroup']['Bundle']['Extensions']['AssetList']['AssetFile'],start=0):
        image['ExternalAssetId'] = match['id'] + '_' + tm[num]

    
    out = xmltodict.unparse(doc, pretty=True)
    with open(round_dir + "/SPORTS_LATAM_CDF_" + match['id'] + "_BUNDLE_ADD.xml", 'a') as file:
        file.write(out)
    
    if match['vod']:
        with open('templates/SPORTS_LATAM_CDF_xxxx_EVENT_REPLAY_EPISODE_ADD.xml') as fd2:
            doc2 = xmltodict.parse(fd2.read())

        doc2['Asset']['HouseId'] = match['id'] + 'replay'
        doc2['Asset']['Title'] = match_title
        doc2['Asset']['TitleBrief'] = match_title
        doc2['Asset']['ParentObjectList']['ParentObjectId'] = match['id']
        doc2['Asset']['SearchKeywordList']['Keyword'][0] = team1
        doc2['Asset']['SearchKeywordList']['Keyword'][1] = team2

        doc2['Asset']['MultilangMetadataList']['MultilangMetadata'][0]['Title'] = match_title
        doc2['Asset']['MultilangMetadataList']['MultilangMetadata'][1]['Title'] = match_title
        doc2['Asset']['EMFAttributeList']['EMFAttribute'][0]['Value'] = match['id']
        doc2['Asset']['EMFAttributeList']['EMFAttribute'][5]['Value'] = match['id']
        doc2['Asset']['EMFAttributeList']['EMFAttribute'][10]['Value'] = match['id']
        
        doc2['Asset']['EMFAttributeList']['EMFAttribute'][4]['Value'] = match['calculatedDate']
        doc2['Asset']['EMFAttributeList']['EMFAttribute'][9]['Value'] = match['calculatedDate']
        doc2['Asset']['EMFAttributeList']['EMFAttribute'][14]['Value'] = match['calculatedDate']

        for platform in doc2['Asset']['Extensions']['PlatformList']['Platform']:
            platform['VideoUrl'] = match['vodData']['url']
            platform['PosterAsset'] = match['id'] + '01'

        out2 = xmltodict.unparse(doc2, pretty=True)
        with open(round_dir + "/SPORTS_LATAM_CDF_" + match['id'] + '01' + "_EVENT_REPLAY_EPISODE_ADD.xml", 'a') as file2:
            file2.write(out2)
        
        url = HIGH + str(match['id'])

        response = requests.request("GET", url, headers=headers, data=payload)
        data2 = response.json()
        
        for i, stream in enumerate(data2, start=2) :
            if stream['type'] == 'COMPACT_SECOND_HALF' or stream['type'] == 'COMPACT_FIRST_HALF':
                gen(doc2, match, stream, 'HIGHLIGHT', i, round_dir)
                #doc2['Asset']['HouseId'] = match['id'] + str(i)
                #doc2['Asset']['Title'] = stream['title']
                #doc2['Asset']['TitleBrief'] = stream['title']
                #doc2['Asset']['ObjectSubtype'] = 'HIGHLIGHT'
                #for platform in doc2['Asset']['Extensions']['PlatformList']['Platform']:
                #    platform['VideoUrl'] = stream['url']
                #    platform['PosterAsset'] = match['id'] + str(i)
                #out3 = xmltodict.unparse(doc2, pretty=True)
                #with open(round_dir + "/SPORTS_LATAM_CDF_" + match['id'] + str(i) + "_HIGHLIGHT_EPISODE_ADD.xml", 'a') as file3:
                #    file3.write(out2)
            elif stream['type'] == 'SCORE' :
                gen(doc2, match, stream, 'GOAL', i, round_dir)
                #doc2['Asset']['HouseId'] = match['id'] + str(i)
                #doc2['Asset']['Title'] = stream['title']
                #doc2['Asset']['TitleBrief'] = stream['title']
                #doc2['Asset']['ObjectSubtype'] = 'GOAL'
                #for platform in doc2['Asset']['Extensions']['PlatformList']['Platform']:
                #    platform['VideoUrl'] = stream['url']
                #    platform['PosterAsset'] = match['id'] + str(i)
                #out3 = xmltodict.unparse(doc2, pretty=True)
                #with open(round_dir + "/SPORTS_LATAM_CDF_" + match['id'] + str(i) + "_GOAL_EPISODE_ADD.xml", 'a') as file3:
                #    file3.write(out2)
    else:
        with open('templates/SPORTS_LATAM_CDF_xxxx_LIVE_EVENT_EPISODE_ADD.xml') as fd3:
            doc3 = xmltodict.parse(fd3.read())
        try:
            match_start = match['event']['start']
            match_end = match['event']['end']
            event_id = str(match['event']['id'])
        except:
            match_start = match['calculatedDate']
            match_end = match['calculatedDate']
            event_id = '_pending_'
        doc3['Asset']['HouseId'] = match['id'] + event_id
        
        doc3['Asset']['Title'] = match_title
        doc3['Asset']['TitleBrief'] = match_title
        doc3['Asset']['ParentObjectList']['ParentObjectId'] = match['id']
        doc3['Asset']['SearchKeywordList']['Keyword'][0] = team1
        doc3['Asset']['SearchKeywordList']['Keyword'][1] = team2

        doc3['Asset']['MultilangMetadataList']['MultilangMetadata'][0]['Title'] = match_title
        doc3['Asset']['MultilangMetadataList']['MultilangMetadata'][1]['Title'] = match_title
        doc3['Asset']['EMFAttributeList']['EMFAttribute'][0]['Value'] = match['id']
        doc3['Asset']['EMFAttributeList']['EMFAttribute'][4]['Value'] = match['id']
        doc3['Asset']['EMFAttributeList']['EMFAttribute'][8]['Value'] = match['id']
        
        doc3['Asset']['Properties']['Property']['StreamStartDate'] = match_start
        doc3['Asset']['Properties']['Property']['StreamEndDate'] = match_end

        for platform in doc3['Asset']['Extensions']['PlatformList']['Platform']:
            try:
                platform['VideoUrl'] = match['event']['recording']['apps'][0]['stream']['streamUrl']
                platform['PosterAsset'] = match['id'] + event_id

            except:
                platform['VideoUrl'] = "https://no-url"
                platform['PosterAsset'] = match['id'] + event_id

        
        out3 = xmltodict.unparse(doc3, pretty=True)
        with open(round_dir + "/SPORTS_LATAM_CDF_" + match['id'] + event_id + "_LIVE_EVENT_EPISODE_ADD.xml", 'a') as file2:
            file2.write(out3)       

def gen(doc,match,stream,type, i, round_dir):
    doc['Asset']['HouseId'] = match['id'] + str(i)
    doc['Asset']['Title'] = stream['title']
    doc['Asset']['TitleBrief'] = stream['title']
    doc['Asset']['ObjectSubtype'] = type
    for platform in doc['Asset']['Extensions']['PlatformList']['Platform']:
        platform['VideoUrl'] = stream['url']
        platform['PosterAsset'] = match['id'] + str(i)
    out = xmltodict.unparse(doc, pretty=True)
    with open(round_dir + "/SPORTS_LATAM_CDF_" + match['id'] + str(i) + "_"+type+"_EPISODE_ADD.xml", 'a') as file:
        file.write(out)

def main():
    for i in range(1, 34):
        Path('round_' + str(i)).mkdir(parents=True, exist_ok=True)
        url = 'https://api.cdf.firesport.io/championship/722-2020/footballdate/722-2020-1-' + str(i) + '?include=event,video'

        response = requests.request("GET", url, headers=headers, data=payload)
        print(response)
        data = response.json()
        for match in data:
            create_bundle(match, str(i), 'round_'+str(i))

  

if __name__ == '__main__': 
    main()
