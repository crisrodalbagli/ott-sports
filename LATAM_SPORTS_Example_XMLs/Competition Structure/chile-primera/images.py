# A script of migrating data from firesports.io to the new latam sports platform 

# focus for this is competition, teams, and logos for these 

import requests
import locale
import json 
import os
import dateutil.parser
import xmltodict
from PIL import Image, ImageDraw, ImageFilter, ImageFont

locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')

TEAM_IMAGE_URL = 'http://images.firesport.io/cdf/team/'
TEAM_API = 'https://api.cdf.firesport.io/team/'
COMP_API = 'https://api.cdf.firesport.io/championship/'
COMP_IMAGE_URL = 'http://images.firesport.io/o/cdf/championship/'

payload = {}
headers = {
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI1NDBlNGM5ZC0yMzQ4LTQwMGYtOWMzNS0xNjMxZDEyYTQxMDQiLCJ1c2VySWQiOiI1Y2VkNjc2NjMzNzJkZTM3ZmMxZjJiMzUiLCJub21icmUiOiJHdWVzdCIsImFwZWxsaWRvUGF0ZXJubyI6IlR1cm5lciIsImZvdG8iOiIiLCJmZWNoYV9uYWNpbWllbnRvIjoiMTkwMC0wMS0wMSIsImZlY2hhTmFjaW1pZW50byI6IjE5MDAtMDEtMDEiLCJnZW5lcm8iOiJmZW1lbmlubyIsImVtYWlsIjoidHVybmVyZ3Vlc3RfMDFAdHVybmVyLmNvbSIsImVzdGFkbyI6ImNvbXBsZXRhZG8iLCJwYXNhcmVsYSI6ImN1cG9uIiwibWF0ZXJpYWwiOiJGdWxsIiwiZGlzcG9zaXRpdm9zIjoiMSIsImlzcyI6ImFwaS5jZGYuY2wiLCJhcHBJZCI6ImVjZGYiLCJyZW1lbWJlciI6dHJ1ZSwiaWF0IjoxNTg2MTA3NTU3LCJleHAiOjE1ODYxOTM5NTd9.M8YAGDtKG4SEO6Nv0LGJmg2WMWRvd5-6eeJuxjrBB2GDvVTzxy3rQJ2uVgn4R2ngTN8nPnfqUFjCFxpwx4BjB755gqe2KAQ0HTuDM2F9Y79198--zA5pMNAjbPLZb1ON_AuYlqezpdAAstaNtAxFDO5iimyO1CU8SHhmYMlg5G2SwEsiv_iDt0kbDoXXLHk1oX7lbwhnYCuTe68nuAuGwa_Ep8hMTglbsjYTstWFd-2bam6V5V-Uy8ju3wVO4RWPK2NXcVEIeNdXeLgl2oMhY7AsZ4EdbZpjX6bBq55lW2dg_WzzaeZjH3PjesATy19QwGfDhg5EJMEEM6pUHIrftA'
}
type_img = ['3_1_Hero', '16_9_Hero', '4_3_Hero','1_1_Badge', '16_9_Tile', '1_1_Tile']

size_hero = 1920, 640
size_hero2 = 1920, 1080
size_hero3 = 1200, 900
size_badge = 360, 360
size_tile = 1024, 576
size_tile2 = 1024, 1024
size2 = [size_hero, size_hero2, size_hero3,
         size_badge, size_tile, size_tile2]

def write_img(id, type2):
    if type2 == 'team':
        try:
            img_data = requests.get(TEAM_IMAGE_URL + str(id) + '/image.png').content
            with open( 'team/'+str(id)+'/'+'image.png', 'wb') as handler:
                handler.write(img_data)
        except:
            print('image error')

    else : 
        img_data = requests.get(COMP_IMAGE_URL + str(id) + '/logo/web.png').content
        with open('competition/'+str(id)+'/'+'image.png', 'wb') as handler:
            handler.write(img_data)


def write_json(team):
    team2 = {}
    team2['id'] = team['id']
    team2['name'] = team['name']
    team2['sport'] = 'Futbol'
    team2['colors'] = ['0x343234','0x3234234']
    team2['logos'] = TEAM_IMAGE_URL + str(team['id']) + '/image.png'
    team2['sponsors'] = [{'name' : 'coca-cola', 'logo' : 'image_url'}]

    with open( 'team/'+str(team['id'])+'.json', 'w') as outfile:
        json.dump(team2, outfile)




def create_images(match,index):
    print(match['id'])
    _dir = 'images2/' + 'SPORTS_LATAM_CDF_'+ str(match['id'])
    write_dir('SPORTS_LATAM_CDF_' + match['id'], 'images2')
    team1_id = match['localTeam']['id']
    team2_id = match['visitorTeam']['id']
    comp_id = match['championship']['id']
    _id = match['id']
   
    im1 = Image.open('../../../data_feeds/team/' + str(team1_id) + '/image.png')
    im2 = Image.open('../../../data_feeds/team/' + str(team2_id) + '/image.png')
    im3 = Image.open('../../../data_feeds/competition/' +
                     str(comp_id) + '/image.png')
    im3a = Image.open('../../../data_feeds/competition/' +
                     str(comp_id) + '/image2.png')
    im4 = Image.open('../../../data_feeds/team/' +
                     str(team1_id) + '/stadium.png')
    try :
        im4_3_1 = Image.open('../../../data_feeds/premira/' +
                         str(team1_id) + '/stadium-3-1.png')
        im4_4_3 = Image.open('../../../data_feeds/premira/' +
                         str(team1_id) + '/stadium-4-3.png')
    except:
        im4_3_1 = Image.open('../../../data_feeds/premira/2630/stadium-3-1.png')
        im4_4_3 = Image.open('../../../data_feeds/premira/2630/stadium-4-3.png')

    im5 = Image.open('backgroun5.png')
    im6 = Image.open('background2.png')
    im7 = Image.open('UpComingMatch-Vs.png')
    im8 = Image.open('UpComingMatch-Back1.jpeg')
    im9 = Image.open('UpComingMatch-Back2.jpeg')
    im10 = Image.open('UpComingMatch-Back3.jpeg')
    im11 = Image.open('backgrround-3-1.png')
    im12 = Image.open('backgound-new3.jpg')
    im13 = Image.open('backgound-new2.png')
    im13_3_1 = Image.open('backgound-new2-3-1.png')
    im13_4_3 = Image.open('backgound-new2-4-3.png')

    im14 = Image.open('highlights1.jpg')
    im15 = Image.open('highlights2.jpg')
    im16 = Image.open('highlights4.jpg')

    im17 = Image.open('filter.png')
    im18 = Image.open('compacto.png')

    im19 = Image.open('background-txt.png')
    im20 = Image.open('sample-png-images-6-transparent.jpg')

    imgvod = [im14, im15, im16]

    size_hero = 1920, 640
    size_hero2 = 1920, 1080
    size_hero3 = 1200, 900
    size_badge = 360, 360
    size_tile = 1024, 576
    size_tile2 = 1024, 1024
    
    get_concat_h_blank(size_hero2,  im1, im2, im3a, im4, im13,  im6, im7, im20,[
                       match['localTeam']['name'], match['visitorTeam']['name'], match['stadium']]).save(_dir+'/SPORTS_LATAM_CDF_' + str(match['id']) + '_' + type_img[1]+'.png')

    get_concat_h_blank2(size_hero,  im1, im2, im3a, im4, im13_3_1, im4_3_1, im7, im20).save(
        _dir+'/SPORTS_LATAM_CDF_' + str(match['id']) + '_' + type_img[0]+'.png')
    
    get_concat_h_blank3(size_hero3,  im1, im2, im3a, im4, im13_4_3, im4_4_3, im7, im20).save(
        _dir+'/SPORTS_LATAM_CDF_' + str(match['id']) + '_' + type_img[2]+'.png')
    #get_concat_h_blank4(size_badge,  im1, im2, im3, im4, im5, im6).save(
    #    _dir+'/' + str(match['id']) + '_' + type_img[3]+'.png')
    imgs = [im8,im9,im10]

    get_concat_h_blank5(size_tile,  im1, im2, im3a, im4, im5, imgs[index % 3], im7, im20, match['calculatedDate']).save(
        _dir+'/SPORTS_LATAM_CDF_' + str(match['id']) + '_' + type_img[4]+'.png')
    
    if (match['vod']):
        get_concat_h_blank_vod(size_tile,  im1, im2, im3a, im18, im5, imgvod[index % 3], im17, im20, [match['localGoals'], match['visitorGoals']]).save(
            _dir+'/SPORTS_LATAM_CDF_' + str(match['id']) + '_VOD_' + type_img[4]+'.png')

    #get_concat_h_blank6(size_tile2,  im1, im2, im3, im4, im5, im6).save(
    #    _dir+'/' + str(match['id']) + '_' + type_img[5]+'.png')

    print('image created')


def get_concat_h_blank(size, im1, im2, im3,im4,im5, im6,im7,im8,detail, color=(1, 65, 92)):
    #dst = Image.new('RGB', (im1.width + im2.width+400,max(im1.height+300, im2.height+300)), color)
        font2 = ImageFont.truetype("Roboto/Roboto-Medium.ttf", 60)

        dst = im4
        dst.thumbnail(size_hero2, Image.ANTIALIAS)
        im7.thumbnail(size_hero2, Image.ANTIALIAS)
        im1.thumbnail((500, 500), Image.ANTIALIAS)
        im2.thumbnail((500, 500), Image.ANTIALIAS)
        im3.thumbnail((150, 150), Image.ANTIALIAS)
        im5.thumbnail((1940, 1100), Image.ANTIALIAS)
        dst.paste(im5, (0, 0), mask=im5)
        #dst.paste(im8, (0, 0))

        #dst.paste(im7, (450, 255), mask=im7)
        dst.paste(im1, (200, 250), mask=im1)
        dst.paste(im2, (1200,250), mask=im2)
        #dst.paste(im3, (875,325), mask=im3)
        #draw = ImageDraw.Draw(dst)
        #draw.text((350, 650), detail[0],
        #          (255, 255, 255), font=font2)
        #draw.text((1200, 650), detail[1],
        #          (255, 255, 255), font=font2)
        #draw.text((825, 900), detail[2],
        #          (255, 255, 255), font=font2)
        return dst

def get_concat_h_blank2(size, im1, im2, im3, im4, im5, im6, im7, im8, color=(1, 65, 92)):
        dst = im6
        dst.thumbnail(size_hero, Image.ANTIALIAS)
        im7.thumbnail(size_hero2, Image.ANTIALIAS)
        im1.thumbnail((300, 300), Image.ANTIALIAS)
        im2.thumbnail((300, 300), Image.ANTIALIAS)
        im3.thumbnail((200, 100), Image.ANTIALIAS)
        #dst.paste(im7, (450, -50), mask=im7)
        dst.paste(im5, (0, 0), mask=im5)
        dst.paste(im1, (350, 150), mask=im1)
        dst.paste(im2, (1350, 150), mask=im2)
        dst.paste(im3, (925, 500), mask=im3)
        #dst.paste(im8, (0, 0), mask=im8)

        return dst


def get_concat_h_blank3(size, im1, im2, im3, im4, im5, im6, im7, im8,color=(1, 65, 92)):
        #1200x900
        dst = im6
        dst.thumbnail(size_hero3, Image.ANTIALIAS)
        im1.thumbnail((300, 300), Image.ANTIALIAS)
        im2.thumbnail((300, 300), Image.ANTIALIAS)
        #im3.thumbnail((100, 300), Image.ANTIALIAS)
        dst.paste(im5, (0, 0), mask=im5)
        dst.paste(im1, (80, 300), mask=im1)
        dst.paste(im2, (800, 300), mask=im2)
        #dst.paste(im8, (0, 0), mask=im8)

        #dst.paste(im3, (450, 440), mask=im3)
        return dst


def get_concat_h_blank4(size, im1, im2, im3, im4, im5, im6, color=(1, 65, 92)):

        dst = im6
        dst.thumbnail(size_badge, Image.ANTIALIAS)
        im1.thumbnail((100, 100), Image.ANTIALIAS)
        im2.thumbnail((100, 100), Image.ANTIALIAS)
        im3.thumbnail((20, 8), Image.ANTIALIAS)
        dst.paste(im1, (50, 50), mask=im1)
        dst.paste(im2, (200, 50), mask=im2)
        dst.paste(im3, (150, 200), mask=im3)
        return dst


def get_concat_h_blank5(size, im1, im2, im3, im4, im5, im6, im7, im8, d,color=(1, 65, 92)):

        dst = im6
        im7.thumbnail(size_tile, Image.ANTIALIAS)
        #
        #draw = ImageDraw.Draw(img)


        # font = ImageFont.truetype(<font-file>, <font-size>)
        font = ImageFont.truetype("Roboto/Roboto-BlackItalic.ttf", 45)
        font2 = ImageFont.truetype("Roboto/Roboto-Black.ttf", 45)

        # draw.text((x, y),"Sample Text",(r,g,b))
        #draw.text((0, 0), "Sample Text", (255, 255, 255), font=font)

        dst.thumbnail(size_tile, Image.ANTIALIAS)
        im1.thumbnail((219, 219), Image.ANTIALIAS)
        im2.thumbnail((219, 219), Image.ANTIALIAS)
        #im4.thumbnail((0, 0), Image.ANTIALIAS)
        dst.paste(im7, (0, 0), mask=im7)
        dst.paste(im1, (60, 195), mask=im1)
        dst.paste(im2, (746, 195), mask=im2)
        draw = ImageDraw.Draw(dst)
        draw.text((230, 30), "Campeonato Nacional 2020",
                  (255, 255, 255), font=font)
        draw.text((320, 480), dateutil.parser.parse(str(d)).strftime('%d  %b   |   %H:%M'),
                  (255, 255, 255), font=font2)
        #dst.paste(im8, (0, 0), mask=im8)

        #dst.paste(im3, (400, 450), mask=im3)
        return dst


def get_concat_h_blank_vod(size, im1, im2, im3, im4, im5, im6, im7,im8, d, color=(1, 65, 92)):

        dst = im6
        im7.thumbnail(size_tile, Image.ANTIALIAS)
        #
        #draw = ImageDraw.Draw(img)

        # font = ImageFont.truetype(<font-file>, <font-size>)
        font = ImageFont.truetype("Roboto/Roboto-BlackItalic.ttf", 45)
        font2 = ImageFont.truetype("Roboto/Roboto-Black.ttf", 200)

        # draw.text((x, y),"Sample Text",(r,g,b))
        #draw.text((0, 0), "Sample Text", (255, 255, 255), font=font)

        dst.thumbnail(size_tile, Image.ANTIALIAS)
        im1.thumbnail((219, 219), Image.ANTIALIAS)
        im2.thumbnail((219, 219), Image.ANTIALIAS)
        #im3.thumbnail((0, 0), Image.ANTIALIAS)
        dst.paste(im7, (0, 0), mask=im7)
        dst.paste(im4,(0, 0), mask=im4)
        dst.paste(im1, (60, 150), mask=im1)
        dst.paste(im2, (746, 150), mask=im2)
        draw = ImageDraw.Draw(dst)
        draw.text((330, 280), str(d[0]),
                  (255, 255, 255), font=font2)
        draw.text((570, 280), str(d[1]),
                  (255, 255, 255), font=font2)
        #dst.paste(im8, (0, 0), mask=im8)

        #dst.paste(im3, (400, 450), mask=im3)
        return dst
def get_concat_h_blank6(size, im1, im2, im3, im4, im5, im6, color=(1, 65, 92)):

        dst = im4
        dst.thumbnail(size_tile2, Image.ANTIALIAS)
        im1.thumbnail((250, 250), Image.ANTIALIAS)
        im2.thumbnail((250, 250), Image.ANTIALIAS)
        im3.thumbnail((80, 30), Image.ANTIALIAS)
        dst.paste(im1, (125, 400), mask=im1)
        dst.paste(im2, (700, 400), mask=im2)
        dst.paste(im3, (400, 750), mask=im3)
        return dst


def write_dir(id, type2):
    try:
        os.mkdir(type2 + '/' + str(id))
        print("Directory ", str(id),  " Created ")
    except FileExistsError:
              print("Directory ", str(id),  " already exists")

def main():
    for i in range(1, 17):
        url = 'https://api.cdf.firesport.io/championship/370-2020/footballdate/370-2020-1-' + str(i) + '?include=event,video'

        response = requests.request("GET", url, headers=headers, data=payload)
        data = response.json()
        for i,match in enumerate(data, start=0):

            create_images(match,i)

  

if __name__ == '__main__': 
    main()
