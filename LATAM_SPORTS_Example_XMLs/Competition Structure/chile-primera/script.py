# A script of migrating data from firesports.io to the new latam sports platform 

# focus for this is competition, teams, and logos for these 

import requests
import json 
import os
import xmltodict

TEAM_IMAGE_URL = 'http://images.firesport.io/cdf/team/'
TEAM_API = 'https://api.cdf.firesport.io/team/'
COMP_API = 'https://api.cdf.firesport.io/championship/'
COMP_IMAGE_URL = 'http://images.firesport.io/o/cdf/championship/'
PROGRMS = 'https://api.cdf.firesport.io/video/?type=PROGRAM&limit=16'
MATCH = 'https://api.cdf.firesport.io/video/1da6ef8d-76c3-41c0-bd73-880d5a2bdacb?include=match'
COMPLETE_MATCH = 'https://api.cdf.firesport.io/video/?team=2641&type=COMPLETE_MATCH&limit=16'
'https://api.cdf.firesport.io/video/?team=2641&type=COMPACT_SECOND_HALF,COMPACT_MATCH,COMPACT_OVERTIME,COMPACT_SECOND_OVERTIME&limit=16'
'https://api.cdf.firesport.io/video/?team=2641&type=SCORE&limit=16'
'https://api.cdf.firesport.io/program_featured/?include=program'
'https://api.cdf.firesport.io/video/?featured=true&limit=10'
'https://api.cdf.firesport.io/video/?team=2641&type=COMPACT_SECOND_HALF,COMPACT_MATCH,COMPACT_OVERTIME,COMPACT_SECOND_OVERTIME&limit=16'


payload = {}
headers = {
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI1NDQzZjU3NC0wOWQ1LTQ4MzktOGVjYS01M2M2MTI0NTgxMGMiLCJ1c2VySWQiOiI1Y2VkNjc2NjMzNzJkZTM3ZmMxZjJiMzUiLCJub21icmUiOiJHdWVzdCIsImFwZWxsaWRvUGF0ZXJubyI6IlR1cm5lciIsImZvdG8iOiIiLCJmZWNoYV9uYWNpbWllbnRvIjoiMTkwMC0wMS0wMSIsImZlY2hhTmFjaW1pZW50byI6IjE5MDAtMDEtMDEiLCJnZW5lcm8iOiJmZW1lbmlubyIsImVtYWlsIjoidHVybmVyZ3Vlc3RfMDFAdHVybmVyLmNvbSIsImVzdGFkbyI6ImNvbXBsZXRhZG8iLCJwYXNhcmVsYSI6ImN1cG9uIiwibWF0ZXJpYWwiOiJGdWxsIiwiZGlzcG9zaXRpdm9zIjoiMSIsImlzcyI6ImFwaS5jZGYuY2wiLCJhcHBJZCI6ImVjZGYiLCJyZW1lbWJlciI6dHJ1ZSwiaWF0IjoxNTg1NzI3MjM2LCJleHAiOjE1ODU4MTM2MzZ9.jvfVgez3eyYUmEHm4swSMc_F6gFTNK8lLT0_k8YNo0GhEmhdPlh00zAS1hHlLT7ilK_zqHpvm2cd5ko3aA81-hW_Ng0YmojTroJUapd5eQD5CVr49NPB0PnXVuB7X0iQOpbnbIzcUKhru7vF_Akbqn0CKTeJMMVezGVD13hHHe59QtqEzmhoLCO0KOwpRH4Vh7GtAKjjx6h-FdcYkvPZjZNc8nAdh71otcbr_VuSaoIGkiYjiUfBFvGHN98Q6X2RMDYO1Tt5UP6un3BsKF8J_64SYrWQRhQjHHarprwWbq96nOV9ldWuCG-5yWfGADHshu2ZvOcuT1ipVrCIZavx0w'
}



def write_img(id, type2):
    if type2 == 'team':
        try:
            img_data = requests.get(TEAM_IMAGE_URL + str(id) + '/image.png').content
            with open( 'team/'+str(id)+'/'+'image.png', 'wb') as handler:
                handler.write(img_data)
        except:
            print('image error')

    else : 
        img_data = requests.get(COMP_IMAGE_URL + str(id) + '/logo/web.png').content
        with open('competition/'+str(id)+'/'+'image.png', 'wb') as handler:
            handler.write(img_data)

def write_json(team):
    team2 = {}
    team2['id'] = team['id']
    team2['name'] = team['name']
    team2['sport'] = 'Futbol'
    team2['colors'] = ['0x343234','0x3234234']
    team2['logos'] = TEAM_IMAGE_URL + str(team['id']) + '/image.png'
    team2['sponsors'] = [{'name' : 'coca-cola', 'logo' : 'image_url'}]

    with open( 'team/'+str(team['id'])+'.json', 'w') as outfile:
        json.dump(team2, outfile)


def create_bundle(match, round, round_dir):
    match_title = match['localTeam']['name'] + \
        ' vs ' + match['visitorTeam']['name']
    team1_id = match['localTeam']['id']
    team2_id = match['visitorTeam']['id']
    team1_name = match['localTeam']['name']
    team2_name = match['visitorTeam']['name']
    with open('SPORTS_LATAM_CDF_517160_BUNDLE_ADD.xml') as fd:
        doc = xmltodict.parse(fd.read())

    doc['BundleGroup']['Bundle']['HouseId'] = match['id']
    doc['BundleGroup']['Bundle']['Title'] = match['localTeam']['name'] + \
        ' vs ' + match['visitorTeam']['name']
    doc['BundleGroup']['Bundle']['UITitle'] = match_title
    doc['BundleGroup']['Bundle']['SearchKeywordList']['Keyword'][0] = team1_id + '_' + team1_name
    doc['BundleGroup']['Bundle']['SearchKeywordList']['Keyword'][1] = team2_id + '_' + team2_name
    doc['BundleGroup']['Bundle']['SearchKeywordList']['Keyword'][2] = 'TEAM_Home_' + \
        team1_id + '_' + team1_name
    doc['BundleGroup']['Bundle']['SearchKeywordList']['Keyword'][3] = 'TEAM_Away_' + \
        team2_id  + '_' + team2_name
    #TEAM_Home_TeamId_TeamName

    doc['BundleGroup']['Bundle']['MultilangMetadataList']['MultilangMetadata'][0]['Title'] = match_title
    doc['BundleGroup']['Bundle']['MultilangMetadataList']['MultilangMetadata'][1]['Title'] = match_title

    out = xmltodict.unparse(doc, pretty=True)
    with open(round_dir + "/SPORTS_LATAM_CDF_" + match['id'] + "_BUNDLE_ADD.xml", 'a') as file:
        file.write(out)
    
    with open('/templates/SPORTS_LATAM_CDF_51715901_EVENT_REPLAY_EPISODE_ADD.xml') as fd2:
        doc2 = xmltodict.parse(fd2.read())

    doc2['Asset']['HouseId'] = match['id'] + '01'
    doc2['Asset']['Title'] = match_title
    doc2['Asset']['TitleBrief'] = match_title
    doc2['Asset']['ParentObjectList']['ParentObjectId'] = match['id']
    doc2['Asset']['SearchKeywordList']['Keyword'][0] = team1_id + \
        '_' + team1_name
    doc2['Asset']['SearchKeywordList']['Keyword'][1] = team2_id + \
        '_' + team2_name
    doc2['Asset']['SearchKeywordList']['Keyword'][2] = 'Home_' + \
    team1_id + '_' + team1_name
    doc2['Asset']['SearchKeywordList']['Keyword'][3] = 'Away_' + \
    team2_id + '_' + team2_name

    doc2['Asset']['MultilangMetadataList']['MultilangMetadata'][0]['Title'] = match_title
    doc2['Asset']['MultilangMetadataList']['MultilangMetadata'][1]['Title'] = match_title
    doc2['Asset']['EMFAttributeList']['EMFAttribute'][0]['Value'] = match['id']
    doc2['Asset']['EMFAttributeList']['EMFAttribute'][5]['Value'] = match['id']
    doc2['Asset']['EMFAttributeList']['EMFAttribute'][10]['Value'] = match['id']
    for platform in doc2['Asset']['Extensions']['PlatformList']['Platform']:
        platform['VideoUrl'] = match['vodData']['url']
    
    out2 = xmltodict.unparse(doc2, pretty=True)
    with open("round2/SPORTS_LATAM_CDF_" + match['id'] + '01' + "_EVENT_REPLAY_EPISODE_ADD.xml", 'a') as file:
        file.write(out2)

def write_dir(id, type2):
    try:
        os.mkdir(type2 + '/'+ str(id))
        print("Directory ", str(id),  " Created ")
    except FileExistsError:
              print("Directory ", str(id),  " already exists")

def main():

    for i in range(1,34):
        os.mkdir('round_' +str(i))
        url = 'https: // api.cdf.firesport.io/championship/370-2020/footballdate/370-2020-1-' + \
                str(i) + '?include=event,video'

        response = requests.request("GET", url, headers=headers, data=payload)
        data = json.load(response)
        for match in data:
            create_bundle(match,str(i), 'round_'+str(i))
      


if __name__ == '__main__': 
    main()
