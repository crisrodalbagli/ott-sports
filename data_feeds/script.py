# A script of migrating data from firesports.io to the new latam sports platform 

# focus for this is competition, teams, and logos for these 

import requests
import json 
import os

TEAM_IMAGE_URL = 'http://images.firesport.io/cdf/team/'
TEAM_API = 'https://api.cdf.firesport.io/team/'
COMP_API = 'https://api.cdf.firesport.io/championship/'
COMP_IMAGE_URL = 'http://images.firesport.io/o/cdf/championship/'

payload = {}
headers = {
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI5NzlhNDY0MC03N2VhLTRlMzktYTM2OS0xNzI5ZmMzZGYzNGMiLCJ1c2VySWQiOiI1Y2VkNjc2NjMzNzJkZTM3ZmMxZjJiMzUiLCJub21icmUiOiJHdWVzdCIsImFwZWxsaWRvUGF0ZXJubyI6IlR1cm5lciIsImZvdG8iOiIiLCJmZWNoYV9uYWNpbWllbnRvIjoiMTkwMC0wMS0wMSIsImZlY2hhTmFjaW1pZW50byI6IjE5MDAtMDEtMDEiLCJnZW5lcm8iOiJmZW1lbmlubyIsImVtYWlsIjoidHVybmVyZ3Vlc3RfMDFAdHVybmVyLmNvbSIsImVzdGFkbyI6ImNvbXBsZXRhZG8iLCJwYXNhcmVsYSI6ImN1cG9uIiwibWF0ZXJpYWwiOiJGdWxsIiwiZGlzcG9zaXRpdm9zIjoiMSIsImlzcyI6ImFwaS5jZGYuY2wiLCJhcHBJZCI6ImVjZGYiLCJyZW1lbWJlciI6dHJ1ZSwiaWF0IjoxNTgzMzkzNjAzLCJleHAiOjE1ODM0ODAwMDN9.gjIx8j017_Q2Wdo6VPyeO_Gq7blXPqx6oN4sNg2XL3fjut6KDBnnNnv4dqnsUfNV2vwnf0CoFSLSe7KESUYLcvDrxVEJ8wPYO8ZyuYaoNRhJNCTQOze5U6C2INe2L1sOIioSs77VKr1eNci9xPf04WkL5KhrWtxetRMGGoC3ha46jkz8GURb5cnlyrjXcKUhXhKXtRkAMKr4UCnhCcqpy76uBb28UhZAH-9sk62V4D6HiX1bzfH-o0oyceY4PS9CrFSV45gbuKY8G-8XSSNK_5GMUd2a72T3K_9zRZ784GJd7zy1DRNW3qT3cyrRduzJoHPFvzoylbO7uzd3Sz5P7g'
}

response = requests.request("GET", COMP_API, headers=headers, data=payload)

print(response.text.encode('utf8'))

def write_img(id, type2):
    if type2 == 'team':
        try:
            img_data = requests.get(TEAM_IMAGE_URL + str(id) + '/image.png').content
            with open( 'team/'+str(id)+'/'+'image.png', 'wb') as handler:
                handler.write(img_data)
        except:
            print('image error')

    else : 
        img_data = requests.get(COMP_IMAGE_URL + str(id) + '/logo/web.png').content
        with open('competition/'+str(id)+'/'+'image.png', 'wb') as handler:
            handler.write(img_data)

def write_json(team):
    team2 = {}
    team2['id'] = team['id']
    team2['name'] = team['name']
    team2['sport'] = 'Futbol'
    team2['colors'] = ['0x343234','0x3234234']
    team2['logos'] = TEAM_IMAGE_URL + str(team['id']) + '/image.png'
    team2['sponsors'] = [{'name' : 'coca-cola', 'logo' : 'image_url'}]

    with open( 'team/'+str(team['id'])+'.json', 'w') as outfile:
        json.dump(team2, outfile)

def write_dir(id, type2):
    try:
        os.mkdir(type2 + '/'+ str(id))
        print("Directory ", str(id),  " Created ")
    except FileExistsError:
              print("Directory ", str(id),  " already exists")

def main():
    with open('team.json') as json_file:
        data = json.load(json_file)
        for team in data:
            write_dir(team['id'], 'team')
            write_img(team['id'], 'team')
            write_json(team)        


if __name__ == '__main__': 
    main()
