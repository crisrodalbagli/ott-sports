# Sports feeds static API

## What is it

Static API providing extra metadata about competitions and teams. The API primarily provides team and competition logos/icons and any specific imagery that the current CATALOGUE data model does not provide. 

## What it is NOT

This is not a full-blown dynamic real-time sports feed with enhanced metadata such as stadiums, players and real-time scores.

## Approach 

A data migration approach is taken for the design of this API, where the following was taken into consideration 
* Sourcing the data from the CURRENT  ECDF firesport.io platform 
* Preserving Competition and Team IDs as per the firesport.io platform
* all images and logos are sources from the current platform to ensure parity and AVAILABILITY of assets
 

## Data Schema 

Types:
* Competitions
* Teams

### Competition

* ID : competition identifier ... shall be universal through the system 
* Name : competition name 
* Sport: football
* Season : year of the competition
* Type : [league, cup, ]
* Logos : array of images 
* Sponsors : competition sponsors e.g. AT&T
* tags for extra metadata?

Example 

```javascript
{
        "id": "639-2020",
        "name": "Copa Chile",
        "fullName": "Copa Chile 2020",
        "sport": "football",
        "season": "2020",
        "type": "cup",
        "logos" : [],
        "sponsors" : [''],
        "teams" : [] 
        "tags": null,
    },
```

### Team

* ID : team identifier ... shall be universal through the system 
* Name : team name 
* Sport: football
* Logos : []
* Colors: ['0xffffff', '0x000000']
* Sponsors:  [{'name' : 'sponsor_name', 'logo': 'sponsor_logo'}]


## API Structure

Lists all competitions 

```
HTTP GET https://{env}.latam-sports.wgmp.io/[API path]
```
Environments : dev, stg, prod 
API Paths
* competitions
* competition/teams
* team



*Example*
HTTP GET https://api.cdf.firesport.io/championship/


```javascript
[
    {
        "id": "639-2019",
        "categoryName": "Copa Chile",
        "categoryFullName": "Copa Chile 2019",
        "categoryId": "639",
        "name": "Copa Chile 2019",
        "tags": null,
        "fullName": "Copa Chile 2019",
        "seasonId": "2019"
    },
    {
        "id": "369-2019",
        "categoryName": "Copa Sudamericana",
        "categoryFullName": "Copa Sudamericana 2019",
        "categoryId": "369",
        "name": "Sudamericana 2019",
        "tags": [],
        "fullName": "Copa Conmebol Sudamericana 2019",
        "seasonId": "2019"
    }
]
```


### image and logo locator

The image locations schema is as follows 

 [Image CDN location]/[TYPE]/[ID]/image.png[?sizing parameters]

 Example 


 ## Relationship with Catalogue Schema

 Competition and team IDs are required within the catalogue schemas in the following parameters

 ### Competition 

* The HouseID of the competition is the GOB SHALL be the same as the firesport.io allocated IDs to ensure and smooth transition

### Team

